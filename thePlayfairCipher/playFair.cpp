#include<iostream>
#include<algorithm>
#include<string>
#include<vector>
#include<sstream>
#include<array>
#define ROWS 5           //rows in encryption table
#define COLUMNS 5        //columns in encryption table
using namespace std;
string alpha = "abcdefghijklmnopqrstuvwxyz";  //alphabets

struct coord{   //structure that stores the row, column and entry in the encryption table
	int row;      //row of element in encryption table
	int column;   //column of element in encryption table
	char value;   //element in encryption table
};
vector<coord> points; //structure of vectors that stores a char and its position in the encryption table

void encrypt(vector<vector<char>> key, vector<char> message, vector<coord> points, vector<char> cipher); //table,plain text,struct vector,cipher text
void decrypt(vector<vector<char>> key, vector<char> message, vector<coord> points, vector<char> cipher); //table,plain text,struct vector,cipher text
void eliminateDuplicates(vector<char>& dups);
void generateTable(vector< vector<char> > table, vector<char> line);
void addFiller(vector<char>& txt); 
int main()
{
	vector< vector<char> > encryptionTable;    //2D  encryption key
	points.push_back(coord());   //enter the properties of the structure to the vector

	//message from user, opearted message,key vector,alphabet vector, complete encryption key
	vector<char> plain, text, keyCode, alphaCode, encryptionKey; 
	string keys; //string key
	int choice;  //choose to encrypt or decryt
	bool condition = true;   //prompts encryption
	char element;  //temporary element to store char transfered to encryption table
	int j = 0, z = 0; //iterators
	string message; //message from the user
	cout << "Enter key: ";
	getline(cin, keys);  //get key from user
	cout << "Enter message: ";
	getline(cin, message); //get message from user
	for (int i = 0; i < keys.size(); i++)    //transfer key vector keyCode
	{
		keyCode.push_back(keys[i]);
	}
	for (int i = 0; i < message.size(); i++)  //transfer message to plain vector
	{
		plain.push_back(message[i]);
	}
	for (int k = 0; k < keyCode.size(); k++)     //delete spaces in key
	{
		if (keyCode[k] == ' ')
		{
			keyCode.erase(keyCode.begin() + k);
		}
	}
	for (int k = 0; k < plain.size(); k++)     //delete spaces in message
	{
		if (plain[k] == ' ')
		{
			plain.erase(plain.begin() + k);
		}
	}
	replace(plain.begin(), plain.end(), 'j', 'i');    //replace 'j' with 'i'
	while (j != plain.size() && j + 1 != plain.size())    //dulpicating lettets are separated by 'x'
	{
		if (plain[j] == plain[j + 1])
		{
			plain.insert(plain.begin() + 1 + j, 'x');
		}
		j += 2;
	}
	if (plain.size() % 2 == 1)               //add filler character if plain text size is odd
	{
		plain.insert(plain.end(), 'x');    //if the size of plain text is odd, add filler char 'x'
	}
	for (int i = 0; i < plain.size(); i++)    //print plain text
	{
		cout << plain[i];
	}
	cout << endl;
	eliminateDuplicates(keyCode);    //eliminate duplicates
	for (int i = 0; i < keyCode.size(); i++)
	{
		cout << keyCode[i];
	}
	cout << endl;
	for (int i = 0; i < alpha.size(); i++)   //transfers the alphabet characters to a vector
	{
		alphaCode.push_back(alpha[i]);
	}
	for (int k = 0; k < keyCode.size(); k++)    //eliminates 'j' in the key
	{
		if (keyCode[k] == 'j')
		{
			keyCode.erase(keyCode.begin() + k);
		}
	}
	for (int k = 0; k < alphaCode.size(); k++)        //delete j in the alphabets
	{
		if (alphaCode[k] == 'j')
		{
			alphaCode.erase(alphaCode.begin() + k);
		}
	}
	encryptionKey.reserve(keyCode.size() + alphaCode.size());                  // preallocate memory for the string key code 
	encryptionKey.insert(encryptionKey.end(), keyCode.begin(), keyCode.end());      //insert the key  
	encryptionKey.insert(encryptionKey.end(), alphaCode.begin(), alphaCode.end());    //merge the alphabets with the key
	eliminateDuplicates(encryptionKey);    //eliminates any duplicates in the merged key

	for (int i = 0; i < encryptionKey.size(); i++)    //prints the reusltant key string
	{
		cout << encryptionKey[i];
	}
	cout << endl;
	encryptionTable.resize(ROWS);    //allocate memory for rows
	for (int i = 0; i < ROWS; ++i)
	{
		encryptionTable[i].resize(COLUMNS);   //allocate memory for columns
	}
	for (int i = 0; i < 5; i++)   //transfer the key string to the encryption table 
	{
		for (int j = 0; j < 5; j++)
		{
			element = encryptionKey[z];       //transfer elements char by char
			encryptionTable[i][j] = element;  //insert elements of the string key to 2D array
			z++;
		}
	}
	for (int i = 0; i < ROWS; ++i)     //displays the encryption table
	{
		for (int j = 0; j < COLUMNS; ++j)
		{
			cout << encryptionTable[i][j] << '\t';   //print the key
		}
		cout << endl;
	}
	cout << "1. Encrypt\n" << "2. Decrypt\n";   //choose to encrypt or decrypt
	cin >> choice;
	switch (choice)
	{
	case 1: condition = true;  //set boolean flag
		break;
	case 2: condition = false;
		break;
	default: cout << "Error!" << endl;
	}
	if (condition)  //if flag is true, encrpyt
	{
		encrypt(encryptionTable, plain, points, text);
	}
	if (condition != true)  //if flag is flag is false, decrypt
	{
		decrypt(encryptionTable, plain, points, text);
	}
	cout << endl;
	system("pause");
	return 0;
}
void encrypt(vector<vector<char>> key, vector<char> message, vector<coord> points, vector<char> cipher) //table,plain text,struct vector,cipher text
{
	//iterators
	int l = 0;   
	int k = 0, m = 0, n = 0;
	//search for elements in message in encryption table
	while (k + 1 <= message.size())  
	{
		for (int j = 0; j < 5; j++)
		{
			for (int i = 0; i < 5; i++)
			{
				if (key[j][i] == message[k]) 
				{
					points.push_back({ j, i, message[k] });//if found, push element to vector structure
				}
			}
		}
		k += 1;
		l++;
	}
	points.erase(points.begin()); //erase first elemnt in vector structure since its empty
	//search for elements in the vector structure in encryption table
	//encode the encryption rules
	while (n != message.size() && n + 1 != message.size()) 
	{
		if (points[n].row == points[n + 1].row)  //if digraph letters are in the same row,
		{
			cipher.push_back(key[points[n].row][(points[n].column + 1) % 5]); //replace the with the element on the right
			cipher.push_back(key[points[n + 1].row][(points[n + 1].column + 1) % 5]);
		}
		if (points[n].column == points[n + 1].column) //if digraph letters are in the same column,
		{
			cipher.push_back(key[(points[n].row + 1) % 5][points[n].column]);//replace element with one below it
			cipher.push_back(key[(points[n + 1].row + 1) % 5][points[n + 1].column]);
		}
		if (points[n].row != points[n + 1].row && points[n].column != points[n + 1].column)//if elements from a rectangle
		{
			cipher.push_back(key[points[n].row][points[n + 1].column]);//replace with last element on row of rectangle
			cipher.push_back(key[points[n + 1].row][points[n].column]);
		}
		n += 2;
	}
	for (int i = 0; i < cipher.size(); i++)  //print the generated cipher
	{
		cout << cipher[i];
	}
}
void decrypt(vector<vector<char>> key, vector<char> message, vector<coord> points, vector<char> cipher) 
{
	int l = 0;
	int k = 0, m = 0, n = 0;
	while (k + 1 <= message.size())
	{
		for (int j = 0; j < key[0].size(); j++)
		{
			for (int i = 0; i < key[0].size(); i++)
			{
				if (key[j][i] == message[k])
				{
					points.push_back({ j, i, message[k] });
				}
			}
		}
		k += 1;
		l++;
	}
	points.erase(points.begin()); //erase first elemnt in vector structure since its empty
	//search for elements in the vector structure in encryption table
	//encode the encryption rules
	while (n != message.size() && n + 1 != message.size())
	{
		if (points[n].row == points[n + 1].row)  //if digraph letters are in the same row,
		{
			cipher.push_back(key[points[n].row][((points[n].column) % 5 + 5 - (1 % 5)) % 5]); //replace the with the element on the left
			cipher.push_back(key[points[n + 1].row][((points[n + 1].column) % 5 + 5 - (1 % 5)) % 5]);
		}
		if (points[n].column == points[n + 1].column) //if digraph letters are in the same column,
		{
			cipher.push_back(key[((points[n].row) % 5 + 5 - (1 % 5)) % 5][points[n].column]); //replace element with one above it
			cipher.push_back(key[((points[n + 1].row) % 5 + 5 - (1 % 5)) % 5][points[n + 1].column]);           
		}
		if (points[n].row != points[n + 1].row && points[n].column != points[n + 1].column) //if elements from a rectangle
		{
			cipher.push_back(key[points[n].row][points[n + 1].column]);  //replace with last element on row of rectangle
			cipher.push_back(key[points[n + 1].row][points[n].column]);
		}
		n += 2;
	}
	for (int i = 0; i < cipher.size(); i++)    //print the generated cipher
	{
		cout << cipher[i];
	}
}
void eliminateDuplicates(vector<char>& dups) //dups is short for duplicates
{
	for (int i = 0; i<dups.size(); i++)
	{
		for (int j = i + 1; j < dups.size() - 1; j++)
		{
			if (dups[i] == dups[j])          //compare elements in the key
			{
				dups.erase(dups.begin() + j);    //erase duplicated elements
			}
		}
	}
}
void addFiller(vector<char>& txt)
{
	int j = 0;
	while (j != txt.size() && j + 1 != txt.size())          //add filler characters
	{
		if (txt[j] == txt[j + 1])
		{
			txt.insert(txt.begin() + 1 + j, 'x');
		}
		j += 2;
	}
}